<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

Route::get('/project-detail/1', function () {
    return view('detail');
});

Route::post('/email/send/contact', 'EmailController@contact');
