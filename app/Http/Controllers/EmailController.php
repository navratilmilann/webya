<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function contact(Request $request) {

        Mail::to(env('MAIL_USERNAME'))->send(new ContactMail($request->all()));
        return "OK";
    }
}
