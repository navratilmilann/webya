$(document).ready(function () {

    /*
        Events
     */
    $('.popup-container .popup .js-close').on('click', closePopup);
    $(window).on('click', '.cc-dismiss', changelivechatIconPosition);
    $(window).on('load', onLoad);

    /*
        Functions
     */
    function closePopup(event) {

        let popupContainer = $(event.target).parent().parent();

        popupContainer.remove();
    }

    // if cookie bar was shown, live chat icon position goes higher
    function changelivechatIconPosition(event) {

        let liveChatIframe = $('.fb_dialog iframe'),
            banner = $('.cookie-consent');

        if (banner.length)
            liveChatIframe.css('bottom', 65 + 'px');
    }

    function openPopup() {

        let popup = $('.popup-container');

        if (popup && checkPopupFrequency(popup)) {

            window.setTimeout(function () {
                popup.show();
            }, 5000);
        }
    }

    function checkPopupFrequency(popup) {

        let popupFrequency = popup.data('frequency'),
            popupId = popup.attr('id'),
            lastOpened = localStorage.getItem(popupId),
            today = getCurrentDate();

        if (popupFrequency === 'daily') {                           // popup will be opened once per day
            if (lastOpened !== today) {
                localStorage.setItem(popupId, today);
                return true;
            }
            return false;
        }
    }

    function getCurrentDate() {
        let today = new Date();
        return today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    }

    function setClientCarousel() {

        let owl = $('.clients .owl-carousel');

        owl.owlCarousel({
            items: 6,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            touchDrag: false,
            mouseDrag: false,
            responsive: {
                0: {
                    items: 2
                },
                768: {
                    items: 6
                },
            }
        });
    }

    function onLoad(event) {

        openPopup();

        setClientCarousel();

        // changelivechatIconPosition(event);
    }
});
