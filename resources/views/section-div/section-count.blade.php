<div class="col-md-6 d-md-flex align-items-md-stretch">
    <div class="count-box">
        <i class="{{$icon}}"></i>
        <span data-toggle="counter-up">{{$counterUp}}</span>
        <p><strong>{{$title}}</strong> {{$text}}</p>
    </div>
</div>
