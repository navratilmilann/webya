<div class="col-lg-5 col-md-4 col-6 m-auto item">
    <a href="{{ $url }}" target="_blank"><img src="{{$img}}" class="img-fluid {{$class ?? ''}}" alt="{{$alt}}" data-aos="zoom-in" data-aos-delay="{{$delay}}"></a>
</div>
