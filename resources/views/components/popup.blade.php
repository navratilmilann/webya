<div class="popup-container overlay" id="{{ $popupId }}"
     data-frequency="{{ $frequency ?? 'daily' }}">
    {{ $slot }}
    <a class="js-close close" href="#">&times;</a>
</div>
