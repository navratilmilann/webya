Ahoj <i>{{ env('APP_NAME') ?? 'Webya' }}</i>,<br>
<br>
{!! $data['message'] !!}<br>
<br>
S pozdravom,<br>
<br>
<i>{{ $data['name'] }}</i>
