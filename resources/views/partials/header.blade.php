<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">

        <div class="logo mr-auto">
            <h1 class="text-light">
                <a href="{{ url('/') }}">
                    <img src="{{asset('img/logo/webia-logo-u.png')}}" alt="Webya logo" class="img-fluid">
                </a>
            </h1>
        </div>
        @include('partials.navigation')
    </div>
</header>
