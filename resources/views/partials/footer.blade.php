<footer class="page-footer font-small">
    <div class="container">
        <div class="footer-copyright text-center py-3">
            <a href="https://www.facebook.com/Webya.sk/" class="social-link"><i
                    class="icofont-facebook"></i></a>
{{--            <a href="https://www.instagram.com/webya.sk/" class="social-link"><i--}}
{{--                    class="icofont-instagram"></i></a>--}}
            <span>Copyright &copy; <?= date("Y"); ?>
            <a href="{{ url('/') }}"> Webya</a></span>
        </div>
    </div>
</footer>
