<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177872749-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-177872749-1');
    </script>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Webya</title>
    <!-- Primary Meta Tags -->
    <meta name="title" content="Máme pre vás softvérové riešenia | Tvorba webových stránok | Webya.sk">
    <meta name="description" content="Špecializujeme sa na tvorbu webových stránok (prezentačné, e-shopy, webové aplikácie) a na nachádzanie adekvátnych softvérových riešení. ">
    <meta name="keywords" content="webová stránka, e-shop, prezentačná stránka, správa webovej stránky, mobilné aplikácie">
    <meta name="robots" content="index, follow">
    <meta name="language" content="Slovak">
    <meta name="author" content="Webya.sk">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.facebook.com/Webya.sk/">
    <meta property="og:title" content="Máme pre vás softvérové riešenia | Tvorba webových stránok | Webya.sk">
    <meta property="og:description" content="Špecializujeme sa na tvorbu webových stránok (prezentačné, e-shopy, webové aplikácie) a na nachádzanie adekvátnych softvérových riešení. ">


    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('/site.webmanifest') }}">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CRaleway:300,300i,400,400i,500,500i,600,600i,700,700i%7CPoppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/venobox/venobox.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/aos/aos.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
