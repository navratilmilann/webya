<nav class="nav-menu d-none d-lg-block">
    <ul>
        <li class="active"><a href="#intro">{{ __('Home') }}</a></li>
        <li><a href="#about">{{__('About')}}</a></li>
        <li><a href="#services">{{__('Services')}}</a></li>
        <li><a href="#portfolio">{{__('Portfolio')}}</a></li>
        <li><a href="#testimonials">{{__('Testimonials')}}</a></li>
        <li><a href="#contact">{{__('Contact')}}</a></li>
    </ul>
</nav>
