<section id="counts" class="counts">
    <div class="container">
        <div class="row">
            <div
                class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-xl-start"
                data-aos="fade-right" data-aos-delay="150">
                <img src="{{ asset('img/counts-img.svg') }}" alt="" class="img-fluid count-img">
            </div>
            <div class="col-xl-7 d-flex align-items-stretch pt-4 pt-xl-0" data-aos="fade-left"
                 data-aos-delay="300">
                <div class="content d-flex flex-column justify-content-center">
                    <div class="row">
                        @include('section-div.section-count', ['icon' => 'icofont-simple-smile','counterUp' => '13',
                            'title' => 'Šťastný klienti', 'text' => 'sú pre nás prioritou. Málo ale kvalitne.'])
                        @include('section-div.section-count', ['icon' => 'icofont-document-folder','counterUp' => '65',
                            'title' => 'LOL,', 'text' => 'Absolútne neviem čo tu napísať. Absolútne neviem čo tu napísať'])
                        @include('section-div.section-count', ['icon' => 'icofont-clock-time','counterUp' => '2936',
                            'title' => 'Hodín', 'text' => 'strávených písaním kódu. Číslo je samozrejme orientačné.'])
                        @include('section-div.section-count', ['icon' => 'icofont-award','counterUp' => '7',
                            'title' => 'Technológie: ', 'text' => 'HTML, CSS, JS, PHP, Laravel, Wordpress, Swift'])
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
