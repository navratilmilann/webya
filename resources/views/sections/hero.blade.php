<section id="intro" class="d-flex align-items-center">
    <div class="container">
        <div class="row">
            <div id="myhero" class="col-lg-6 pt-5 pt-lg-4 order-2 order-lg-1 d-flex flex-column justify-content-center">
                <h1 data-aos="fade-up">{{ __('We Can Build') }}<br>{{ __('Your Website') }}</h1>
                <h3 class="subtitle" data-aos="fade-up" data-aos-delay="400">
                    {{__('Software Solutions')}}
                </h3>
                <div class="button-container" data-aos="fade-up" data-aos-delay="800">
                    <a href="#portfolio" class="primary-button scrollto">{{__('Our Portfolio')}}</a>
                    <a href="https://youtu.be/LGQowW80JHw" class="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"><i class="icofont-play-alt-2"></i> {{__('Video About Us')}}</a>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left" data-aos-delay="200">
                <img src="{{ asset('img/webya-hero.png') }}" class="img-fluid animated" alt="Webya.sk">
            </div>
        </div>
    </div>
</section>
