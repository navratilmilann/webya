<section id="portfolio" class="portfolio">
    <div class="container">

        @include('section-div.section-title', ['title' => __('Portfolio')])

        <div class="row" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-12 d-flex justify-content-center">
                <ul id="portfolio-flters">
                    <li data-filter="*" class="filter-active">{{__('All')}}</li>
                    <li data-filter=".filter-web">{{__('Web')}}</li>
                    <li data-filter=".filter-eshop">{{__("E-shop")}}</li>
                </ul>
            </div>
        </div>
        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="400">
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                <div class="portfolio-wrap">
                    <img src="{{ asset('img/portfolio/Valeria_Polcova.png') }}" class="img-fluid" alt="valeriapolcova">
                    <div class="portfolio-info">
                        <h4>ValeriaPolcova.sk</h4>
                        <p>Web</p>
                        <div class="portfolio-links">
                            <a href="{{ asset('img/portfolio/Valeria_Polcova-full.png') }}" data-gall="portfolioGallery"
                               class="venobox"
                               title="valeriapolcova.sk"><i class='bx bx-zoom-in'></i></a>
                            <a href="www.valeriapolcova.sk" target="_blank" title="valeriapolcova.sk"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-eshop">
                <div class="portfolio-wrap">
                    <img src="{{ asset('img/portfolio/Dupik.png') }}" class="img-fluid" alt="dupik">
                    <div class="portfolio-info">
                        <h4>Dupik.sk</h4>
                        <p>E-shop</p>
                        <div class="portfolio-links">
                            <a href="{{ asset('img/portfolio/Dupik-full.png') }}" data-gall="portfolioGallery"
                               class="venobox"
                               title="dupik.sk"><i class='bx bx-zoom-in'></i></a>
                            <a href="www.dupik.sk" target="_blank" title="dupik.sk"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                <div class="portfolio-wrap">
                    <img src="{{ asset('img/portfolio/Sdla.png') }}" class="img-fluid" alt="sdla">
                    <div class="portfolio-info">
                        <h4>SDLA.sk</h4>
                        <p>{{__('Web (maintenance)')}}</p>
                        <div class="portfolio-links">
                            <a href="{{ asset('img/portfolio/Sdla-full.png') }}" data-gall="portfolioGallery"
                               class="venobox"
                               title="sdla.sk"><i class='bx bx-zoom-in'></i></a>
                            <a href="www.sdla.sk" target="_blank" title="sdla.sk"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                <div class="portfolio-wrap">
                    <img src="{{ asset('img/portfolio/Todosystem.png') }}" class="img-fluid" alt="fatless">
                    <div class="portfolio-info">
                        <h4>TodoSystem.isite.sk</h4>
                        <p>{{__('Web app')}}</p>
                        <div class="portfolio-links">
                            <a href="{{ asset('img/portfolio/Todosystem-full.png') }}" data-gall="portfolioGallery"
                               class="venobox"
                               title="TodoSystem.isite.sk"><i class='bx bx-zoom-in'></i></a>
                            <a href="www.todosystem.isite.sk" target="_blank" title="fatless.sk"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-eshop">
                <div class="portfolio-wrap">
                    <img src="{{ asset('img/portfolio/Drevenemotyliky-marseen.png') }}" class="img-fluid"
                         alt="drevenemotyliky-marseen">
                    <div class="portfolio-info">
                        <h4>DreveneMotyliky-Marseen.sk</h4>
                        <p>E-shop</p>
                        <div class="portfolio-links">
                            <a href="{{ asset('img/portfolio/Drevenemotyliky-marseen-full.png') }}"
                               data-gall="portfolioGallery"
                               class="venobox"
                               title="DreveneMotyliky-Marseen.sk"><i class='bx bx-zoom-in'></i></a>
                            <a href="www.drevenemotyliky-marseen.sk" target="_blank"
                               title="drevenemotyliky-marseen.sk"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                <div class="portfolio-wrap">
                    <img src="{{ asset('img/portfolio/hanami.png') }}" class="img-fluid" alt="hanami-sushi">
                    <div class="portfolio-info">
                        <h4>HANAMI-Sushi.sk</h4>
                        <p>Web</p>
                        <div class="portfolio-links">
                            <a href="{{ asset('img/portfolio/Hanami-full.png') }}" data-gall="portfolioGallery"
                               class="venobox"
                               title="HANAMI-Sushi.sk"><i class='bx bx-zoom-in'></i></a>
                            <a href="hanami-sushi.sk" target="_blank" title="hanami-sushi.sk"><i
                                    class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                <div class="portfolio-wrap">
                    <img src="{{ asset('img/portfolio/fatless.png') }}" class="img-fluid" alt="fatless">
                    <div class="portfolio-info">
                        <h4>FatLess.sk</h4>
                        <p>{{__('Web app')}}</p>
                        <div class="portfolio-links">
                            <a href="{{ asset('img/portfolio/Fatless-full.png') }}" data-gall="portfolioGallery"
                               class="venobox"
                               title="FatLess.sk"><i class='bx bx-zoom-in'></i></a>
                            <a href="www.fatless.sk" target="_blank" title="fatless.sk"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
