<section id="clients" class="clients">
    <div class="container">
        <div class="row d-flex justify-content-center owl-carousel">
            @include('section-div.section-client', ['img' => 'img/clients/hanami.png', 'alt' => 'Hanami Sushi', 'delay' => '100', 'url' => 'www.hanami-sushi.sk/'])
            @include('section-div.section-client', ['img' => 'img/clients/sdla.png', 'alt' => 'Small Danube Language Academy', 'delay' => '200', 'url' => 'www.sdla.sk/'])
            @include('section-div.section-client', ['img' => 'img/clients/valeria.png', 'alt' => 'Valéria Polčová', 'delay' => '300', 'url' => 'www.valeriapolcova.sk'])
            @include('section-div.section-client', ['img' => 'img/clients/dupik.png', 'alt' => 'Detský obchodík', 'delay' => '400', 'url' => 'www.dupik.sk/'])
            @include('section-div.section-client', ['img' => 'img/clients/marseen.png', 'alt' => 'Marseen Hanmade motýliky', 'delay' => '500', 'url' => 'www.www.drevenemotyliky-marseen.sk/'])
            @include('section-div.section-client', ['img' => 'img/clients/fatless.png', 'alt' => 'Fatless - osobný tréner vo vrecku', 'delay' => '500', 'url' => 'www.fatless.sk/', 'class' => 'height-oriented mx-auto'])
            @include('section-div.section-client', ['img' => 'img/clients/you.png', 'alt' => 'Toto miesto je rezervované pre vás', 'delay' => '700', 'url' => '#'])
        </div>
    </div>
</section>
