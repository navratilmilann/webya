<section id="about" class="about">
    <div class="container">

        @include('section-div.section-title', ['title' => __('About')])

        <div class="row content">
            <div class="col-lg-8 offset-lg-2 text-center" data-aos="fade-up" data-aos-delay="150">
                <p>
					{{ __('We specialize') }}
                </p>
            </div>
        </div>
    </div>
</section>
