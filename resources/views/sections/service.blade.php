<section id="services" class="services section-bg">
    <div class="container">
        @include('section-div.section-title', ['title' => __('Services')])

        <div class="row">
            <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class='bx bxs-window-alt'></i></div>
                    <h4 class="title">{{ __('Websites and apps') }}</h4>
                    <p class="description">{{__('We specialize in unique')}}</p>

                </div>
            </div>
            <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class='bx bxs-mobile'></i></div>
                    <h4 class="title">{{__('Mobile apps')}}</h4>
                    <p class="description">{{__('You can also entrust us')}}</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon"><i class='bx bxs-rocket'></i></div>
                    <h4 class="title">{{__('Marketing and management')}}</h4>
                    <p class="description">{{__('We do have a marketing')}}</p>
                </div>
            </div>
        </div>
    </div>
</section>
