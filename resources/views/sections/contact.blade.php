<section id="contact" class="contact">
    <div class="container">

        @include('section-div.section-title', ['title' => __('Contact')])

        <div class="row justify-content-center">
            <p class="font-weight-bold text-center" data-aos="fade-up" data-aos-delay="100">
                {{__('contact info')}}
            </p>
            <div class="col-lg-3 col-sm-4" data-aos="fade-up" data-aos-delay="100">
                <div class="contact-about">
                    <div class="info pt-3">
                        <div>
                            <i class="ri-mail-send-line"></i>
                            <p><a href="mailto:info@webya.sk">info@webya.sk</a></p>
                        </div>
                        <div>
                            <i class="ri-phone-line"></i>
                            <p><a href="tel:+421908234026">+421 949 497 599</a></p>
                        </div>
                        <div>
                            <i class='bx bxs-briefcase'></i>
                            <p>IČO: 51830400 <br> DIČ: 1121622667</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-8 form-container pt-3" data-aos="fade-up" data-aos-delay="300">
                <form action="{{ url('/email/send/contact') }}" method="post" class="php-email-form">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Meno"
                               data-rule="minlen:4" data-msg="Please enter at least 4 chars"/>
                        <input type="email" class="form-control" name="email" id="email"
                               placeholder="Email" data-rule="email"
                               data-msg="Please enter a valid email"/>
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject"
                               placeholder="Predmet" data-rule="minlen:4"
                               data-msg="Please enter at least 8 chars of subject"/>
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                                <textarea class="form-control" name="message" rows="5" data-rule="required"
                                          data-msg="Please write something for us" placeholder="Správa"></textarea>
                        <div class="validate"></div>
                    </div>
                    <div class="mb-3">
                        <div class="loading">Odosielam</div>
                        <div class="error-message">Vyskytol sa problém...</div>
                        <div class="sent-message">Vaša správa bola odoslaná, čoskoro sa ozveme!</div>
                    </div>
                    <div class="text-right">
                        <button type="submit">Odoslať</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
