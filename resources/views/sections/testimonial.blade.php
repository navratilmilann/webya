<section id="testimonials" class="testimonials section-bg">
    <div class="container">

        @include('section-div.section-title', ['title' => __('Testimonials')])

        <div class="owl-carousel testimonials-carousel" data-aos="fade-up" data-aos-delay="200">
            <div class="testimonial-wrap">
                <div class="testimonial-item">
                    <img src="{{ asset('img/testimonials/testimonial-valeria.png') }}" class="testimonial-img"
                         alt="Valéria Polčová">
                    <h3>Valéria Polčová</h3>
                    <h4>{{__('Singer')}}</h4>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        {{__('Polcova testimonials')}}
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>
            </div>
            <div class="testimonial-wrap">
                <div class="testimonial-item">
                    <img src="{{ asset('img/testimonials/testimonials-5.jpg') }}" class="testimonial-img" alt="">
                    <h3>Matej</h3>
                    <p>
                        <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                        {{__('Matej testimonials')}}
                        <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
