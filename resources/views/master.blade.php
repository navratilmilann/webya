<!DOCTYPE html>
<html lang="sk">

@include('partials.head')

@yield('styles')

<body>
@include('cookieConsent::index')
@include('partials.header')

@yield('content')

@include('partials.footer')

<!-- Discount popup -->
{{--<x-popup>--}}
{{--    <!-- passing parameters to slot -->--}}
{{--    <x-slot name="popupId">--}}
{{--        discount-popup--}}
{{--    </x-slot>--}}
{{--    <div class="popup">--}}
{{--        <h2>--}}
{{--            &lt;Buď medzi prvými 5!&gt;--}}
{{--        </h2>--}}
{{--        <img src="{{ asset('img/discount.svg') }}" class="popup-image" alt="discount image">--}}
{{--        <div class="content">--}}
{{--            Iba teraz máte jedinečnú príležitosť využiť <b>zľavu 10%</b> na prezentačnú stránku od Webyi. <br>--}}
{{--            Pre využitie zľavy je potrebné uplatniť promo kód <b>webya-10</b> .--}}
{{--        </div>--}}
{{--        <a href="#contact" class="primary-button button scrollto js-close">Áno, mám záujem</a>--}}
{{--        <a class="cancel-button button js-close">Teraz nie</a>--}}
{{--    </div>--}}
{{--</x-popup>--}}

@include('partials.script')

@yield('scripts')
</body>

</html>
