@extends('master')

@section('content')

    @include('sections.hero')

    <main id="main">
        @include('sections.client')
        @include('sections.about')
{{--        @include('sections.count')--}}
        @include('sections.service')
        @include('sections.portfolio')
        @include('sections.testimonial')
        @include('sections.contact')
        {{--    @include('sections.more-service')--}}
        {{--    @include('sections.feature')--}}
        {{--    @include('sections.team')--}}
        {{--    @include('sections.pricing')--}}
        {{--    @include('sections.faq')--}}
    </main>

@endsection
