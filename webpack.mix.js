const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/script.js', 'public/js')
    .sass('resources/scss/main.scss', 'public/css');
    // .sourceMaps();

mix.copyDirectory('resources/fonts', 'public/fonts');
//
// if (!mix.inProduction()) {
//     mix.webpackConfig({devtool: "inline-source-map"});
// }
